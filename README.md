# README

## Ruby version

Ruby - 2.2.1
Rails - 4.2.4

## System dependencies

PhantomJS (used by Polterigesit).

`brew install phantomjs`

## Configuration

Setup mongoid database

`cp config/mongoid.sample.yml config/mongoid.yml`

## Database creation

There is no SQL database. No migrations/database creation needed.

## How to run the test suite

Javascript code is covered by feature tests(these should be testes with jasmin in the future)

`bundle exec rspec`.
