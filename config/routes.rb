Rails.application.routes.draw do
  root 'main#index'
  namespace :api, defaults: {format: :json} do
    namespace :v1 do
      resources :calculations, only: [:create]
    end
  end
  get '*anything' => redirect('/')
end
