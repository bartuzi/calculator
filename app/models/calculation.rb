class Calculation
  include Mongoid::Document
  include Mongoid::Enum
  include Mongoid::Locker

  before_save :calculate_result

  COMPONENTS_NUMERICALITY_VALIDATORS = {
      only_integer: true,
      greater_than_or_equal_to:  1,
      less_than_or_equal_to: 99
  }.freeze

  field :a, type: Integer
  field :b, type: Integer
  field :result, type: Float
  field :invocation_counter, type: Integer, default: 1
  
  enum :operation, [:sum, :difference, :multiplication, :division], default: nil

  validates :a, presence: true, numericality: COMPONENTS_NUMERICALITY_VALIDATORS
  validates :b, presence: true, numericality: COMPONENTS_NUMERICALITY_VALIDATORS
  validates :invocation_counter, numericality: { greater_than_or_equal_to: 1, only_integer: true }
  validates :operation, presence: true

  def calculate_result
    self.result = a.public_send(operation_method_name, b)
  end

  private

  def operation_method_name
    case operation.to_sym
      when :sum             then :+
      when :difference      then :-
      when :multiplication  then :*
      when :division        then :fdiv
    end
  end
end
