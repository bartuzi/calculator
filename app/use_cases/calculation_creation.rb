class CalculationCreation
  attr_reader :http_status, :calculation

  def initialize(params)
    @params = params
  end

  def perform
    @calculation = Calculation.find_or_initialize_by(params)
    calculation.new_record? ? handle_new_calculation : handle_found_calculation
    calculation
  end

  def successful?
    calculation.present? && calculation.errors.blank?
  end

  private

  attr_accessor :params
  attr_writer :http_status, :calculation

  def handle_found_calculation
    @http_status = :ok
    calculation.with_lock do
      calculation.invocation_counter += 1
      calculation.save
    end
  end

  def handle_new_calculation
    @http_status = calculation.save ? :created : :unprocessable_entity
  end
end
