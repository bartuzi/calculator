app.filter "calculationSummary", ($resource)->
  (calculation)->
    """
    Operation: #{calculation.readable_operation}
    Result: #{calculation.result}
    ID: #{calculation._id}
    Count: #{calculation.invocation_counter}
    """

