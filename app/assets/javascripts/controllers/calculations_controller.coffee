app.controller 'CalculationsCtrl', ($scope, $filter, Calculation)->
  $scope.title = 'Awesome possom calculator'
  $scope.errorMessage = []

  $scope.calculation = { a: 23, b: 2, operation: 'sum' }

  $scope.serverResponse = ()->
    ($scope.calculationResult && $filter('calculationSummary')($scope.calculationResult)) ||
      $scope.errorMessage.join("\n")

  $scope.save = ()->
    $scope.busy = true
    $scope.errorMessage = []
    Calculation.create($scope.calculation,
      (successResult)->
        $scope.calculationResult = successResult
      , (failResult)->
        $scope.calculationResult = null
        _.each failResult.data.errors, (errors, key)->
          _.each errors, (error) ->
            $scope.errorMessage.push("#{key}: #{error}")
        failResult
    ).$promise.then ()->
      $scope.busy = false
    , ()->
      $scope.busy = false

