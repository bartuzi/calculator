module Api
  module V1
    class CalculationsController < Api::BaseController
      respond_to :json

      def create
        creator = CalculationCreation.new(calculation_params)
        @calculation = creator.perform.decorate
        respond_with(@calculation, status: creator.http_status, location: root_url)
      end

      private

      def calculation_params
        params.require(:calculation).permit(:a, :b, :operation)
      end
    end
  end
end
