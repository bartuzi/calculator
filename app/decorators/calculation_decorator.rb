class CalculationDecorator < Draper::Decorator
  delegate_all

  def readable_operation
    "#{a} #{friendly_operation_symbol} #{b}"
  end

  def as_json(option = {})
    super({}).merge({ readable_operation: readable_operation })
  end

  private

  def friendly_operation_symbol
    case operation.to_sym
      when :sum             then '+'
      when :difference      then '-'
      when :multiplication  then '*'
      when :division        then '/'
    end
  end
end
