require 'spec_helper'

describe CalculationDecorator do
  let(:undecorated_calculation) { build_stubbed(:calculation, a: 1, b: 2) }
  let(:calculation) { undecorated_calculation.decorate }

  describe '#as_json' do
    subject { calculation.as_json }
    before do
      allow(calculation).to receive(:readable_operation) { 'anything' }
    end

    it 'adds #readable_operation result to JSON object' do
      expect(subject).to eq(calculation.as_json.merge(
        {
            readable_operation: 'anything'
        }
      ))
    end
  end

  describe '#readable_operation' do
    subject { calculation.readable_operation }
    context 'when operation is division' do
      before { allow(calculation).to receive(:operation) { :division } }

      it { is_expected.to eq('1 / 2') }
    end

    context 'when operation is summary' do
      before { allow(calculation).to receive(:operation) { :sum } }

      it { is_expected.to eq('1 + 2') }
    end

    context 'when operation is multiplication' do
      before { allow(calculation).to receive(:operation) { :multiplication } }

      it { is_expected.to eq('1 * 2') }
    end

    context 'when operation is division' do
      before { allow(calculation).to receive(:operation) { :difference } }

      it { is_expected.to eq('1 - 2') }
    end
  end
end
