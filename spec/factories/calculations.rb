FactoryGirl.define do
  factory :calculation do
    a 4
    b 3
    operation 'difference'
  end
end
