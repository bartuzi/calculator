require 'rails_helper'

RSpec.feature 'Calculation Creation', type: :feature, js: true do
  background { visit root_path }

  context 'when user fills calculation form with incorrect data' do
    background { fill_form_with_correct_numbers }
    %w(a b).each do |field|
        include_examples 'blocked form', field, 0, '>= 1'
        include_examples 'blocked form', field, 100, '<= 99'
        include_examples 'blocked form', field, nil, 'Required'
    end

    context 'when both fields are incorrect' do
      background do
        fill_in 'a', with: '100'
        fill_in 'b', with: nil
      end
      scenario 'shows error message for both inputs' do
        within('#calculationForm') do
          within('#errors-a') do
            expect(page).to have_content('<= 99')
          end

          within('#errors-b') do
            expect(page).to have_content('Required')
          end
        end
      end
    end

    xscenario 'users cheats on client-side validations' do
      # TODO: Skip angular validations below line below.
      # Changing ng-min attribute doesn't change validations. Still form is invalid
      page.execute_script("$('#a').attr('ng-min', '-1')")
      fill_in 'a', with: -1
      submit_form
      within('#resut') { expect(page).to have_content('a: must be greater than or equal to 1') }
    end
  end

  context 'when user fills calculation form with correct data' do
    background { fill_form_with_correct_numbers }

    scenario 'rest of the form is available' do
      within('#calculationForm') do
        within('#operation-selection') do
          expect(page).not_to have_css('label[disabled]')
        end
        expect(page).to have_css('button#submit')
        expect(page.find('#submit')[:disabled]).to be_falsey
      end
    end

    scenario 'calculation is returned' do
      make_invisible_fields_visible
      within('#result') do
        expect(page.text).to eq('')
      end
      choose 'difference'
      submit_form
      expect(page.find('#result')).to have_content('43 - 27')
      within('#result') do
        expect(page).to have_content('43 - 27')
        expect(page).to have_content('Result: 16')
        expect(page).to have_content(MONGOID_ID_REGEXP)
        expect(page).to have_content(/Count: \d+/)
      end
    end

    xscenario 'user can not re-submit the form before previous calculation is returned' do
      # TODO: AJAX control with angularjs?
      # http://stackoverflow.com/questions/32908390/capybara-expect-before-ajax-request-completed
      expect(page.find('#submit')[:disabled]).to be_falsey
      submit_form
      expect(page.find('#submit')[:disabled]).not_to be_falsey
      ## Here ajax response is received
      expect(page.find('#submit')[:disabled]).to be_falsey
    end
  end
end

def make_invisible_fields_visible
  page.execute_script("$(\"input[type='radio']\").show()")
end

def submit_form
  click_button 'Calculate'
end

def fill_form_with_correct_numbers
  fill_in 'a', with: 43
  fill_in 'b', with: 27
end
