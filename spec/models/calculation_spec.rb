require 'spec_helper'

RSpec.describe Calculation do
  it { is_expected.to have_fields(:a, :b).of_type(Integer) }
  it { is_expected.to have_fields(:invocation_counter).of_type(Integer).with_default_value_of(1) }
  it { is_expected.to validate_presence_of(:a) }
  it { is_expected.to validate_presence_of(:b) }
  it { is_expected.to validate_presence_of(:operation) }
  it do
    is_expected.to validate_numericality_of(:invocation_counter).to_allow(
      {
        only_integer: true,
        greater_than_or_equal_to: 1
      }
    )
  end

  %i(a b).each do |field|
    it do
      is_expected.to validate_numericality_of(field).to_allow(
        {
          only_integer: true,
          greater_than_or_equal_to: 1,
          less_than_or_equal_to: 99
        }
      )
    end
  end

  describe '#calcula  te_result' do
    shared_examples 'calculation results' do |operation, expected_value|
      context "when opeation is #{operation}" do
        let!(:model) { build(:calculation, a: 8, b: 5, operation: operation) }
        subject { model.result }
        before { model.calculate_result }

        it { is_expected.to eq expected_value }
      end
    end

    include_examples 'calculation results', :sum, 13
    include_examples 'calculation results', :difference, 3
    include_examples 'calculation results', :multiplication, 40
    include_examples 'calculation results', :division, 1.6
  end
end
