require 'spec_helper'

MONGOID_ID_REGEXP = /[0-9a-fA-F]{24}/

RSpec.configure do |config|
  config.include Mongoid::Matchers, type: :model
end
