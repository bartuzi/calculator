shared_examples_for 'blocked form' do |field, value, expected_error_message|
  scenario "restricts form when #{field} has #{value} value" do
    fill_in field, with: value
    within('#calculationForm') do
      within("#errors-#{field}") do
        expect(page).to have_content(expected_error_message)
      end
      within('#operation-selection') do
        expect(page).to have_css('label > span[disabled]', count: 4)
      end
      expect(page).to have_css('#submit[disabled]')
    end
  end
end
