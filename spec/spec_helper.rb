require 'database_cleaner'
require 'simplecov'
require 'capybara/rspec'
require 'capybara/poltergeist'
require 'factory_girl'
require 'pry'
require 'rails/mongoid'
require 'mongoid-rspec'
Capybara.javascript_driver = :poltergeist
Dir["./spec/support/**/*.rb"].sort.each { |f| require f}
SimpleCov.start 'rails'
RSpec.configure do |config|
  config.include FactoryGirl::Syntax::Methods

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.pattern = '**/*_spec.rb'

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end
end
