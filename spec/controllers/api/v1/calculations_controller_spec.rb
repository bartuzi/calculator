require 'rails_helper'

RSpec.describe Api::V1::CalculationsController, type: :controller do
  describe 'POST #create' do
    let(:calculation_creation) { instance_double(CalculationCreation) }
    let(:calculation_params) { {a: 2, b: 3, operation: :sum} }
    let(:params) { { calculation: calculation_params, format: :json } }
    let(:calculation) { instance_double(Calculation) }

    before do
      allow(CalculationCreation).to receive(:new) { calculation_creation }
      allow(calculation_creation).to receive(:perform) { calculation }
      allow(calculation).to receive(:decorate)
    end
    subject(:request) { post :create, params }

    context 'when http status is stubbed' do
      before do
        allow(calculation_creation).to receive(:http_status) { 200 }
        request
      end

      it 'decorates model' do
        expect(calculation).to have_received(:decorate)
      end

      it 'uses calculation creation use case' do
        expect(calculation_creation).to have_received(:perform)
      end
    end

    context 'when calculation creation fails' do
      before do
        allow(calculation_creation).to receive(:http_status) { :unprocessable_entity }
        request
      end
      it 'responds with 422 header' do
        expect(calculation_creation).to have_received(:http_status)
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context 'when new calculation is created' do
      before do
        allow(calculation_creation).to receive(:http_status) { :created }
        request
      end
      it 'responds with 201 header' do
        expect(response).to have_http_status(:created)
      end
    end

    context 'when calculation already exists' do
      before do
        allow(calculation_creation).to receive(:http_status) { :ok }
        request
      end
      it 'responds with 200 header' do
        expect(response).to have_http_status(:ok)
      end
    end
  end
end
