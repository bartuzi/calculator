RSpec.describe CalculationCreation do
  let(:calculation_params) { { a: 2, b: 2, operation: 'multiplication' } }
  subject(:new_instance) { described_class.new(calculation_params) }

  describe '#perform' do
    let(:new_calculation) { build(:calculation, calculation_params) }
    let(:perform) { subject.perform }
    context 'when calculation does not exist' do
      before do
        allow(Calculation).to receive(:find_or_initialize_by).with(calculation_params) { new_calculation }
      end

      it 'creates new record' do
        expect{ perform }.to change{ Calculation.count }.by(1)
      end

      it 'sets HTTP status to 201' do
        perform
        expect(subject.http_status).to eq(:created)
      end
    end

    context 'when invalid params are given' do
      before do
        allow(Calculation).to receive(:find_or_initialize_by).with(calculation_params) { new_calculation }
        allow(new_calculation).to receive(:save) { false }
      end

      it 'sets HTTP status to 422' do
        perform
        expect(subject.http_status).to eq(:unprocessable_entity)
      end

      it 'does not create new record' do
        expect{ perform }.not_to change { Calculation.count }
      end
    end

    context 'when calculation exists' do
      let(:existing_calculation) { create(:calculation, calculation_params) }
      before do
        allow(Calculation).to receive(:find_or_initialize_by).with(calculation_params) { existing_calculation }
        allow(existing_calculation).to receive(:with_lock) { |&block| block.call }
      end

      it 'does not create new record' do
        expect{ perform }.not_to change { Calculation.count }
      end

      it 'increases invocations count' do
        expect{ perform }.to change { existing_calculation.invocation_counter }.by(1)
      end

      it 'sets HTTP status to 200' do
        perform
        expect(subject.http_status).to eq(:ok)
      end
    end
  end

  describe '#successful?' do
    subject { new_instance.successful? }
    context 'when #perform has not been called yet' do
      it { is_expected.to eq(false) }
    end

    context 'when #perform has been already called' do
      let(:calculation) { build_stubbed(:calculation) }
      before do
        allow(new_instance).to receive(:calculation) { calculation }
      end

      context 'when calculation has errors' do
        before { allow(calculation).to receive(:errors) { {any: 'hash'} } }

        it { is_expected.to eq(false) }
      end

      context 'when calculation does not have errors' do
        before { allow(calculation).to receive(:errors) { {} } }

        it { is_expected.to eq(true) }
      end
    end
  end
end
